#!/usr/bin/python

from __future__ import print_function
from bcc import BPF
from struct import pack
from socket import inet_ntop, ntohs, AF_INET, AF_INET6

# define BPF program
bpf_text = """
#include <uapi/linux/ptrace.h>
#define KBUILD_MODNAME "foo"
#include <net/tcp.h>
#include <linux/tcp.h>
#include <uapi/linux/tcp.h>
#include <uapi/linux/ip.h>
#include <uapi/linux/ipv6.h>
#include <bcc/proto.h>

BPF_STACK_TRACE(stack_traces, 1024);
struct ipv4_data_t {
    u32 pid;
    u64 ip;
    u32 saddr;
    u32 daddr;
    u16 sport;
    u16 dport;
	char task[TASK_COMM_LEN];
	u32 stack_id;
	u32 num;
};
BPF_PERF_OUTPUT(ipv4_events);

struct ipv6_data_t {
    u32 pid;
    u64 ip;
    unsigned __int128 saddr;
    unsigned __int128 daddr;
    u16 sport;
    u16 dport;
	char task[TASK_COMM_LEN];
	u32 stack_id;
	u32 num;
};
BPF_PERF_OUTPUT(ipv6_events);

struct key_t {
	u32 key;
};
BPF_HASH(count, struct key_t, u32);

static struct tcphdr *skb_to_tcphdr(const struct sk_buff *skb)
{
    // unstable API. verify logic in tcp_hdr() -> skb_transport_header().
    return (struct tcphdr *)(skb->head + skb->transport_header);
}
static inline struct iphdr *skb_to_iphdr(const struct sk_buff *skb)
{
    // unstable API. verify logic in ip_hdr() -> skb_network_header().
    return (struct iphdr *)(skb->head + skb->network_header);
}

int trace_kfree_skb(struct pt_regs *ctx, struct sk_buff *skb)
{    
	struct key_t key = {.key = 0};
	u32 *val, zero = 0;
	val = count.lookup_or_try_init(&key, &zero);
	if (val == NULL)
		return 0;
	(*val)++;
    u32 value = *val;
	u16 sport = 0, dport = 0;
    struct tcphdr *tcp = skb_to_tcphdr(skb);
    struct iphdr *ip = skb_to_iphdr(skb);
	sport = tcp->source;
    dport = tcp->dest;
    sport = ntohs(sport);
    dport = ntohs(dport);
	char version = (*(char *)ip);
	#if defined(__LITTLE_ENDIAN_BITFIELD)
	version = (version >> 4) & 0x0f;
	#elif defined (__BIG_ENDIAN_BITFIELD)
	version = version & 0x0f;
	#endif
	u32 pid = bpf_get_current_pid_tgid() >> 32;
    if (version == 4)
    {
        struct ipv4_data_t data4 = {};
        data4.pid = pid;
        data4.ip = 4;
        data4.saddr = ip->saddr;
        data4.daddr = ip->daddr;
        data4.dport = dport;
        data4.sport = sport;
	data4.num = value;
	bpf_get_current_comm(&data4.task, sizeof(data4.task));
    	data4.stack_id = stack_traces.get_stackid(ctx, 0);
        ipv4_events.perf_submit(ctx, &data4, sizeof(data4));
        return 0;
    } else if (version == 6) {
	struct ipv6_data_t data6 = {};
        data6.pid = pid;
        data6.ip = 6;
	struct ipv6hdr *ip6 = (struct ipv6hdr *)(ip);
        bpf_probe_read(&data6.saddr, sizeof(data6.saddr),
        ip6->saddr.in6_u.u6_addr32);
        bpf_probe_read(&data6.daddr, sizeof(data6.daddr),
        ip6->daddr.in6_u.u6_addr32);
        data6.dport = dport;
        data6.sport = sport;
	data6.num = value;
	bpf_get_current_comm(&data6.task, sizeof(data6.task));
	data6.stack_id = stack_traces.get_stackid(ctx, 0);
        ipv6_events.perf_submit(ctx, &data6, sizeof(data6));
    } //else
	return 0;
}
"""
	
# initialize BPF
b = BPF(text=bpf_text)
b.attach_kprobe(event="kfree_skb", fn_name="trace_kfree_skb")

stack_traces = b.get_table("stack_traces")

# process event
def print_ipv4_event(cpu, data, size):
    event = b["ipv4_events"].event(data)
    print("%-6d %-12.12s %-2d %-20s > %-20s %d" % (
        event.pid, event.task.decode('utf-8', 'replace'), event.ip,
        "%s:%d" % (inet_ntop(AF_INET, pack('I', event.saddr)), event.sport),
        "%s:%d" % (inet_ntop(AF_INET, pack('I', event.daddr)), event.dport), event.num))	
    for addr in stack_traces.walk(event.stack_id):
        sym = b.ksym(addr, show_offset=True)
        print("\t%s" % sym)
    print("")

def print_ipv6_event(cpu, data, size):
    event = b["ipv6_events"].event(data)
    print("%-6d %-12.12s %-2d %-20s > %-20s %d" % (
        event.pid, event.task.decode('utf-8', 'replace'), event.ip,
        "%s:%d" % (inet_ntop(AF_INET6, event.saddr), event.sport),
        "%s:%d" % (inet_ntop(AF_INET6, event.daddr), event.dport), event.num))
    for addr in stack_traces.walk(event.stack_id):
        sym = b.ksym(addr, show_offset=True)
        print("\t%s" % sym)
    print("")
		
# header
print("%-6s %-12s %-2s %-20s %-20s %s" % ("PID", "COMM", "IP", "SADDR:SPORT", "DADDR:DPORT", "COUNT"))

# read events
b["ipv4_events"].open_perf_buffer(print_ipv4_event)
b["ipv6_events"].open_perf_buffer(print_ipv6_event)
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
